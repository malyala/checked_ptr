/*
Name : Checked_ptr.cpp
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:
Unit tests for Checked_ptr class template and member functions.

To do:
Known issues:
Unknown issues:

Version:
0.01 Initial version
0.02 Created testframework for TDD. Added all functionalities.
0.03 Merged into mainline.

Coding Log:
14-07-20 Initial version. Empty project compiles under ISO C++ 14
15-07-20 Created a sketch of class template Checked_ptr
16-07-20 Created test framework
18-07-20 Created functionality of exceptions in TestFramework. Merged working data structure into main line. 
         Made function void CheckRange(std::size_t index) static 
*/

#include "Checked_ptr.h"

/* Tests for intializations and display of pointer array */
void Testone(Checked_ptr<int> *& a);

void Testtwo(Checked_ptr<int> *& a);

void Testthree(Checked_ptr<int> *& a);

//#if(0)
SINT32 main(void)
{
    try
    {
        Checked_ptr<int> * a = new Checked_ptr<int>[30];

        Testone(a);

        // Only one of these can be executed at a time
        //Testtwo(a);
        Testthree(a);

        /* Delete variables */
        delete[] a;
    }
    catch (std::exception const& e)
    {
        std::cout << e.what() << std::endl;
    }
    return 0;
}
//#endif

/* The array is intiailized in this test */
void Testone(Checked_ptr<int> *& a)
{
    /* This should print a error when element 22 is accessed */
    for (int i=0; i<30; i++)
    {
        a[i]= i;
    }
}

/* The array should be intiailized first for this test . Comment CheckRange(this->Currentindex); in TestCheckedptr<T>& operator= (T arg) operator declaration prior to this function call*/
void Testtwo(Checked_ptr<int> *& a)
{
    /* This should print a error when element 22 or a element beyond 30 is accessed */
    for (int i=0; i<30; i++)
    {
        std::cout << a[i] << std::endl;
    }
}

/* The array should be intiailized first for this test . Comment CheckRange(this->Currentindex); in TestCheckedptr<T>& operator= (T arg) operator declaration prior to this function call*/
void Testthree(Checked_ptr<int> *& a)
{
    /* This should print a error when element 22 or a element beyond 30 is accessed */
    for (int i=26; i<35; i++)
    {
        std::cout << a[i] << std::endl;
    }
}
