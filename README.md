# README #

This project creates a checked pointer data structure in ISO C++ 14 which can be used to create a pointer array with range 
checking. This is very useful in safety applications where range of access in arrays should be restricted to avoid errors. 
More features can be added to customize it according to your application. This project compiles with MinGW G++ under ISO C++14.