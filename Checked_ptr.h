/*
Name : Checked_ptr.h
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:
A checked_ptr for creating arrays with range checking.
To do:
Known issues:
Unknown issues:

Version:
0.01 Initial version
0.02 Added static function and moved operator= outside of class
*/
#ifndef CHECKEDPTR_H
#define CHECKEDPTR_H
#ifdef __cplusplus

#include <iostream>
#include "std_types.h"
#include <vector>
#include <memory>
#include <exception>
/*

Name: template <typename T> class Checked_ptr 
Arguments: Type T
Description:
This Template is for creating a checked pointer type which throws
a exception for range access errors in arrays.The data structure should track which element is being accessed in a given range specified by user.
In this case, we will have 30 elements in which one element and any element greater than 30 are barred from
access.This means that if the application accesses element 22 then a exception should be thrown.
The element being accessed must be tracked to throw a exception. The program should not crash.
Initialization of a checked_ptr object is as:
TestChecked_ptr<int> * a = new TestChecked_ptr<int> [30];
When a variable is created in pointer array and memory is allocated for it, assign its index in the variable.
When this variable is accessed, its index would be checked. In this way a out of range array index can be checked
and any index in the array can be checked if was accessed for reading or writing into the array.
*/
template <typename T>
class Checked_ptr {
private:
    T* element;
    std::size_t Currentindex;
    static std::size_t IndexCount;
public:
    /* default  Constructor */
    Checked_ptr<T>(): element{new T{}}
    {
        Currentindex= IndexCount;
        IndexCount++;
    }
    // Check if the data type should be in pointer representation as *a = 1, *(a+4) = 1
    /*Copy assignment of pointer  with single element. */
    Checked_ptr<T>& operator= (T arg);
    /* prefix * (indirection ) operator */
    T& operator* ()
    {
        return *element;
    }
    const T& operator* () const
    {
        return *element;
    }
    T* operator-> ()
    {
        return element;
    }
    const T* operator-> () const
    {
        return element;
    }
    /* Destructor */
    ~Checked_ptr<T>()
    {
        delete element;
    }
    /* for printing value of elem */
    friend std::ostream& operator<<(std::ostream& os, const Checked_ptr<T>& obj)
    {
        Checked_ptr<T>::CheckRange(obj.Currentindex);
        os << *obj;
        return os;
    }
    /* Function to check range */
    static void CheckRange(std::size_t index)
    {
        if (index==21) {
            throw std::runtime_error("Runtime Error: accessed array element 22");
        }
        else if (index>  Checked_ptr<T>::IndexCount) {
            throw std::range_error("Range Error: accessed array elements beyond array size");
        }
    }
};

// Initialize index count
template <typename T>
std::size_t Checked_ptr<T>::IndexCount=0;

/*Copy assignment of pointer  with single element. */
template <typename T>
Checked_ptr<T>& Checked_ptr<T>::operator= (T arg)
{
    /* *elem is array element */
    *element=  arg ;
    //std::cout <<"Address of element: " << element << std::endl;
    //CheckRange(this->Currentindex);
    return *this;
}
   
#endif
#endif // #ifndef CHECKEDPTR_H'