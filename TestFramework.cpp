/*
Name : TestFramework.cpp
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:

A test framework which contains test cases.
Unit tests for class template and member functions.
To do:
Known issues:
Unknown issues:

Version:
0.01 Initial version
*/



#include "TestFramework.h"


/* Tests for intializations and display of pointer array */
void Testone(TestCheckedptr<int> *& a);

void Testtwo(TestCheckedptr<int> *& a);

void Testthree(TestCheckedptr<int> *& a);

//#if(0)
SINT32 main(void)
{
    try
    {
        TestCheckedptr<int> * a = new TestCheckedptr<int>[30];

        Testone(a);

        // Only one of these can be executed at a time
        //Testtwo(a);
        Testthree(a);

        /* Delete variables */
        delete[] a;
    }
    catch (std::exception const& e)
    {
        std::cout <<  e.what()  << std::endl;
    }
    return 0;
}
//#endif

/* The array is intiailized in this test */
void Testone(TestCheckedptr<int> *& a)
{
    /* This should print a error when element 22 is accessed */
    for (int i=0; i<30; i++)
    {
        a[i]= i;
    }
}


/* The array should be intiailized first for this test . Comment CheckRange(this->Currentindex); in TestCheckedptr<T>& operator= (T arg) operator declaration prior to this function call*/
void Testtwo(TestCheckedptr<int> *& a)
{
    /* This should print a error when element 22 or a element beyond 30 is accessed */
    for (int i=0; i<30; i++)
    {
        std::cout << a[i] << std::endl;
    }
}

/* The array should be intiailized first for this test . Comment CheckRange(this->Currentindex); in TestCheckedptr<T>& operator= (T arg) operator declaration prior to this function call*/
void Testthree(TestCheckedptr<int> *& a)
{
    /* This should print a error when element 22 or a element beyond 30 is accessed */
    for (int i=26; i<35; i++)
    {
        std::cout << a[i] << std::endl;
    }
}
