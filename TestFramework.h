/*
Name : TestFramework.h
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:

A test framework which contains test cases.


To do:
Known issues:
Unknown issues:


Version:
0.01 Initial version
*/

#ifndef TESTFRAMEWORK_H
#define TESTFRAMEWORK_H

#ifdef __cplusplus

#include <iostream>
#include "std_types.h"
#include <vector>
#include <algorithm>
#include <memory>
#include <utility>
#include <cstdlib>


/*
Test Template for creating a checked pointer type which throws
a exception for range access errors in arrays.

Note:

The data structure should track which element is being accessed in a given range specified by user.
In this case, we will have 30 elements in which one element and any element greater than 30 are barred from
access.
This means that if the application accesses element 22 then a exception should be thrown.
The element being accessed must be tracked to throw a exception. The program should not crash.

Initialization of a checked_ptr object

TestChecked_ptr<int> * a = new TestChecked_ptr<int> [30];

When a variable is created in pointer array and memory is allocated for it, assign its index in the variable.
When this variable is accessed, its index would be checked. In this way a out of range array index can be checked
and any index in the array can be checked if was accessed for reading or writing into the array.

When this Template works, integrate into mainline.
Create a new << operator within the class template



Notes:
Template being edited.
*/
// The main class would be the container
template <typename T>
class TestCheckedptr
{
private:
    T* element;
    std::size_t Currentindex;
    static std::size_t IndexCount;
    std::size_t var;
public:


    /* default  Constructor */

    TestCheckedptr<T>(): element{new T{}}
    {
        Currentindex= IndexCount;
        IndexCount++;
    }

    // Check if the data type should be in pointer representation as *a = 1, *(a+4) = 1
    /*Copy assignment of pointer  with single element. */
    TestCheckedptr<T>& operator= (T arg)
    {

        /* *elem is array element */
        *element=  arg ;
        //std::cout <<"Address of element: " << element << std::endl;
        //CheckRange(this->Currentindex);
        return *this;
    }



    /* prefix * (indirection ) operator */
    T& operator* ()
    {
        return *element;
    }
    const T& operator* () const
    {
        return *element;
    }


    T* operator-> ()
    {
        return element;
    }


    const T* operator-> () const
    {
        return element;
    }

    /* Destructor */
    ~TestCheckedptr<T>()
    {
        delete element;
        IndexCount=0;
    }

    /* To be defined */
    void clear(void)
    {


    }
    //#if(0)
    /* for printing value of elem */
    friend std::ostream& operator<<(std::ostream& os, const TestCheckedptr<T>& obj)
    {

        TestCheckedptr<T> myobj;

        myobj.CheckRange(obj.Currentindex);
        os << *obj;

        return os;
    }
    //#endif


    /* Function to check range */
    void CheckRange(std::size_t index)
    {
        if (index==21)
        {
           throw std::runtime_error("Runtime Error: accessed array element 22");
        }
        else if (index>=30)
        {
            throw std::range_error("Runtime Error: accessed array elements beyond 30");
        }
    }

    void PrintIndex(void)
    {
        std::cout << "Current index: " << Currentindex << std::endl;
    }
};

// Initialize index count
template <typename T>
std::size_t TestCheckedptr<T>::IndexCount=0;


#endif
#endif